n = 20
c = [13, 1, 11, 10, 6]
import math

def flatlandSpaceStations(n, c):
    maxDistance = 0
    currDistance = 0
    c = sorted(c)
    start = c[0] - 0
    end = n - max(c) - 1
    if n == len(c):
        return 0
    for i in range(1,len(c)):
        currDistance = (c[i] - c[i-1]) // 2
        if currDistance > maxDistance:
            maxDistance = currDistance     
    return max(start, end, maxDistance)

flatlandSpaceStations(n, c)