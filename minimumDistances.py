a = [3,2,1,2,3]

def minimumDistance(a):
    distance = len(a)
    n = len(a)
    for i in range(len(a)):
        for j in range(i + 1, len(a)):
            if a[i] == a[j]:
                distance = min(distance, (j - i))

    return distance

minimumDistance(a)