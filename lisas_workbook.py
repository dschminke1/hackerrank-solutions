n = 5
k = 3
arr = [4, 2, 6, 1, 10]

import math

def lisasWorkbook(n, k, arr):
    special = 0
    page = 1

    for chapter in arr:
        for problem in range(1, chapter + 1):
            if problem == page:
                special +=1
            if problem % k == 0 or problem == chapter:
                page +=1

    return special


lisasWorkbook(n, k, arr)