h = 5
m = 47

def timeInWords(h, m):
    hour_map =  {1:'one', 2: 'two', 3: 'three', 4: 'four',
                5: 'five', 6: 'six', 7: 'seven', 8: 'eight',
                9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',
                13: 'thirteen', 14: 'fourteen', 15: 'quarter', 16: 'sixteen',
                17: 'seventeen', 18: 'eighteen', 19: 'nineteen', 20: 'twenty',
                21: 'twenty one', 22: 'twenty two', 23: 'twenty three', 24: 'twenty four',
                25: 'twenty five', 26: 'twenty six', 27: 'twenty seven', 28: 'twenty eight',
                29: 'twenty nine', 30: 'half', 0: "o' clock"           
                }

    if m == 00:
        time = hour_map[h] + ' ' + hour_map[m]
    elif m == 1:
        time = hour_map[m] + ' minute past ' + hour_map[h]
    elif m == 15:
        time = hour_map[m] + ' past ' + hour_map[h]
    elif m > 00 and m < 30:
        time = hour_map[m] + ' minutes past ' + hour_map[h]
    elif m == 30:
        time = hour_map[m] + ' past ' + hour_map[h]
    elif m == 45:
        m -= 30
        h += 1
        time = hour_map[m] + ' to ' + hour_map[h]
    elif m > 30 and m < 59:
        m = 60 - m
        h +=1
        time = hour_map[m] + ' minutes to ' + hour_map[h] 
        
    print(time)

    return time


timeInWords(h, m)