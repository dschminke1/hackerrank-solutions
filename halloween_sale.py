p = 100
d = 19
m = 1
s = 180

def halloweenSale(p,d,m,s):

    games_bought = []
    count = 0
    while sum(games_bought) < s:
        current = p - (d * count)
        if current > m:
            count +=1
            games_bought.append(current)
        else:
            count +=1
            games_bought.append(m)
    if sum(games_bought) == s:
        return count
    else:
        return count -1


halloweenSale(p,d,m,s)