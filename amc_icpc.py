topic = ['10101', '11110', '00010', '00001']

def acmIcpcTeam(topic):

    n = len(topic)
    m = len(topic[0])
    max_count = 0
    teams = 0
    for i in range(n):
        topic[i] = list(topic[i])
    for i in range(n-1):
        for j in range(i+1, n):
            topic_count = 0
            for k in range(m):
                if topic[i][k] == '1' or topic[j][k] == '1':
                    topic_count +=1
                    if topic_count > max_count:
                        max_count = topic_count
                        teams = 1
                    elif topic_count == max_count:
                        teams +=1
    print(max_count, teams)

acmIcpcTeam(topic)