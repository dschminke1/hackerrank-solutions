B= [4,5,6,7]

def fairRations(B):
    loaves = 0
    odds = 0
    for i in B:
        if i % 2 != 0:
            odds +=1
    if odds % 2 != 0:
        return 'NO'
    else:
        for i in range(len(B) -1):
            if B[i] % 2 != 0:
                B[i] +=1
                B[i + 1] +=1
                loaves +=2
    return str(loaves)

fairRations(B)