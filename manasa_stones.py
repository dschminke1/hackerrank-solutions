n = 73
a = 25
b = 25

def manasa_stones(n, a, b):
    ans = []
    for i in range(n):
        ans.append((a * (n-i-1)) + (b * i))
    return(sorted(list(set(ans))))

manasa_stones(n, a, b)